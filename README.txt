Keek Drupal Module
------------------
"Adds a formatter for text fields, to display embedded Keek videos."

https://drupal.org/project/keek


This modules allows you to embed Keek videos easily.

After installing this module, and enabled it...
2-steps Configuration:

In any content type use or add a Field of the type text.
Then, set Embed Keek Format on Display settings for chosen field.
Simply usage: Paste Keek's post URL on chosen field
e.g. https://www.keek.com/!QslAbab

Live usage: http://bit.ly/14V6rEJ

Further information:

This module comes with standard formats suggested by Vine.
(Yeah! Vine!, no Keek. Use also Vine module and have standarized embeds )
Vine module: https://drupal.org/project/vine

You can choose between three sizes: Large (600px), Medium (480px) and Small (320px)


Include Keeks on your articles and get great social video impact on your websites


A little experience with Drupal was needed for developing this simple module.
Base code was taken from this tutorial
Drupal 7 Tutorial: Creating Custom Formatters with the Field API
http://www.metaltoad.com/blog/drupal-7-tutorial-creating-custom-formatters
by Dann Linn https://drupal.org/user/566584
